#include <casacore/casa/Containers/Record.h>

#include "ska/plasma/casacore_types.h"
#include "ska/plasma/plasma_stman.h"
#include "ska/plasma/plasma_stman_impl.h"


namespace ska {

namespace plasma {

PlasmaStMan::PlasmaStMan(std::string plasma_socket,
  const std::map<std::string, ObjectID> &tensor_object_ids,
  const std::map<std::string, ObjectID> &table_object_ids)
  : pimpl(std::make_unique<PlasmaStMan::impl>(std::move(plasma_socket),
      tensor_object_ids, table_object_ids))
{
}

PlasmaStMan::~PlasmaStMan() = default;

void PlasmaStMan::ping_plasma()
{
	pimpl->ping_plasma();
}

void PlasmaStMan::set_plasma_get_timeout(std::int64_t timeout)
{
	pimpl->set_plasma_get_timeout(timeout);
}

void PlasmaStMan::set_plasma_connect_retries(int connect_retries)
{
	pimpl->set_plasma_connect_retries(connect_retries);
}

DataManager *PlasmaStMan::clone() const
{
	return pimpl->clone();
}

String PlasmaStMan::dataManagerType() const
{
	return pimpl->dataManagerType();
}

String PlasmaStMan::dataManagerName() const
{
	return pimpl->dataManagerName();
}

void PlasmaStMan::create64(rownr_t aNrRows)
{
	pimpl->create64(aNrRows);
}

rownr_t PlasmaStMan::open64(rownr_t aRowNr, AipsIO &ios)
{
	return pimpl->open64(aRowNr, ios);
}

rownr_t PlasmaStMan::resync64(rownr_t aRowNr)
{
	return pimpl->resync64(aRowNr);
}

Bool PlasmaStMan::flush(AipsIO &ios, Bool doFsync)
{
	return pimpl->flush(ios, doFsync);
}

DataManagerColumn *PlasmaStMan::makeScalarColumn(
    const String &aName, int aDataType,
    const String &aDataTypeID)
{
	return pimpl->makeScalarColumn(aName, aDataType, aDataTypeID);
}

DataManagerColumn *PlasmaStMan::makeDirArrColumn(
    const String &aName, int aDataType,
    const String &aDataTypeID)
{
	return pimpl->makeDirArrColumn(aName, aDataType, aDataTypeID);
}

DataManagerColumn *PlasmaStMan::makeIndArrColumn(
    const String &aName, int aDataType,
    const String &aDataTypeID)
{
	return pimpl->makeIndArrColumn(aName, aDataType, aDataTypeID);
}

void PlasmaStMan::deleteManager()
{
	pimpl->deleteManager();
}

void PlasmaStMan::addRow64(rownr_t aNrRows)
{
	pimpl->addRow64(aNrRows);
}

Record PlasmaStMan::dataManagerSpec() const
{
	return pimpl->dataManagerSpec();
}

Record PlasmaStMan::getProperties() const
{
	return pimpl->getProperties();
}

void PlasmaStMan::setProperties(const Record& props)
{
	pimpl->setProperties(props);
}

DataManager *PlasmaStMan::makeObject(
    const String &aDataManType, const Record &spec)
{
	return PlasmaStMan::impl::makeObject(aDataManType, spec);
}

void register_plasmastman()
{
	DataManager::registerCtor("PlasmaStMan", PlasmaStMan::makeObject);
}

} // namespace plasma
} // namespace ska
