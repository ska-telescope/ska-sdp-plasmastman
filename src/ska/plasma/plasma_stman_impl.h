#ifndef SKA_PLASMA_PLASMA_STMAN_IMPL_H
#define SKA_PLASMA_PLASMA_STMAN_IMPL_H

#include <memory>
#include <vector>

#include "ska/plasma/casacore_types.h"
#include "ska/plasma/plasma_client.h"
#include "ska/plasma/plasma_stman.h"

namespace ska {
namespace plasma {

// Forward declaration
class PlasmaStManColumn;
class ArrowObjectInfo;

/**
 * The Plasma-based storage manager implementation
 *
 * This class fully implements the plasma-based storage manager, while
 * PlasmaStMan only exposes this implementation, while hiding its dependencies.
 */
class PlasmaStMan::impl {

private:
	/// The name of this storage manager
	static constexpr const char *DATAMAN_NAME = "PlasmaStMan";
	/// The name of the specification field holding the Tensor column->objectID mapping
	static constexpr const char *SPEC_FIELD_TENSOR_OBJECT_IDS = "TENSOROBJECTIDS";
	/// The name of the specification field holding the Table column->objectID mapping
	static constexpr const char *SPEC_FIELD_TABLE_OBJECT_IDS = "TABLEOBJECTIDS";
	/// The name of the specification field holding the plasma socket
	static constexpr const char *SPEC_FIELD_PLASMA_SOCKET = "PLASMASOCKET";
	/// The name of the property holding the number of connect retries
	static constexpr const char *PROPERTY_PLASMA_CONNECT_RETRIES = "PLASMACONNECTRETRIES";
	/// The name of the property holding the Get timeout
	static constexpr const char *PROPERTY_PLASMA_GET_TIMEOUT = "PLASMAGETTIMEOUT";
	/// The client used to connect to plasma
	PlasmaClient _client;
	/// The columns handled by this storage manager, indexed by column name
	std::vector<std::unique_ptr<PlasmaStManColumn>> _columns;
	/// Mapping from column name to Tensor object ID
	std::map<std::string, ObjectID> _tensor_object_ids;
	/// Mapping from column name to Table object ID
	std::map<std::string, ObjectID> _table_object_ids;
	/// Number of rows used by all columns managed by this storage manager
	rownr_t _nrows = 0;

	void read_metadata(AipsIO &ios);
	void write_metadata(AipsIO &ios);
	DataManagerColumn *makeColumnCommon(const String &aName,
	    int aDataType, const String &aDataTypeID);

	/**
	 * Find the Arrow object information for the given column name. If none
	 * found no_column_mapping_error is raised.
	 * @param column_name A column name.
	 * @return The Arrow object information (Object ID and Arrow object type)
	 * for the given column name.
	 */
	ArrowObjectInfo arrow_info(const std::string &column_name) const;

public:
	/// @see PlasmaStMan::PlasmaStMan
	impl(std::string plasma_socket = "",
	  std::map<std::string, ObjectID> tensor_object_ids = {},
	  std::map<std::string, ObjectID> table_object_ids = {});

	/**
	 * Destructor declaration because of incomplete PlasmaStManColumn type usage
	 * in one of our members; otherwise its implementation is defaulted.
	 */
	~impl();

	/// @see PlasmaStMan::ping_plasma
	void ping_plasma();

	/// @see PlasmaStMan::set_plasma_get_timeout
	void set_plasma_get_timeout(std::int64_t timeout);

	/// @see PlasmaStMan::set_plasma_connect_retries
	void set_plasma_connect_retries(int connect_retries);

	/// @see PlasmaStMan::clone
	DataManager *clone() const;
	/// @see PlasmaStMan::dataManagerType
	String dataManagerType() const;
	/// @see PlasmaStMan::dataManagerName
	String dataManagerName() const;
	/// @see PlasmaStMan::create64
	void create64(rownr_t aNrRows);
	/// @see PlasmaStMan::open64
	rownr_t open64(rownr_t aRowNr, AipsIO &ios);
	/// @see PlasmaStMan::resync64
	rownr_t resync64(rownr_t aRowNr);
	/// @see PlasmaStMan::flush
	Bool flush(AipsIO &, Bool doFsync);
	/// @see PlasmaStMan::makeScalarColumn
	DataManagerColumn *makeScalarColumn(const String &aName,
	  int aDataType, const String &aDataTypeID);
	/// @see PlasmaStMan::makeDirArrColumn
	DataManagerColumn *makeDirArrColumn(const String &aName,
	  int aDataType, const String &aDataTypeID);
	/// @see PlasmaStMan::makeIndArrColumn
	DataManagerColumn *makeIndArrColumn(const String &aName,
	  int aDataType, const String &aDataTypeID);
	/// @see PlasmaStMan::deleteManager
	void deleteManager();
	/// @see PlasmaStMan::addRow64
	void addRow64(rownr_t aNrRows);
	/// @see PlasmaStMan::dataManagerSpec
	Record dataManagerSpec() const;
	/// @see PlasmaStMan::getProperties
	Record getProperties() const;
	/// @see PlasmaStMan::setProperties
	void setProperties (const Record& props);
	/// @see PlasmaStMan::makeObject
	static DataManager *makeObject(
	  const String &aDataManType, const Record &spec);

	/**
	 * Return the number of rows used by all columns managed by this storage
	 * manager
	 *
	 * @return The number of rows used by all columns managed by this storage
	 * manager
	 */
	rownr_t nrows() const
	{
		return _nrows;
	}

};

} // namespace plasma
} // namespace ska

#endif // SKA_PLASMA_PLASMA_STMAN_IMPL_H