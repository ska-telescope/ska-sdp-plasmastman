#ifndef SKA_PLASMA_CASACORE_TYPES_H
#define SKA_PLASMA_CASACORE_TYPES_H

#include <casacore/casa/aips.h>
#include <casacore/casa/BasicSL/Complexfwd.h>

// Forward declaration of casacore types, later aliased into ska::plasma
namespace casacore {
	class AipsIO;
	class ArrayBase;
	class DataManager;
	class DataManagerColumn;
	class IPosition;
	class Record;
	class RefRows;
	class Slicer;
	class String;
} // namespace casacore

namespace ska {
namespace plasma {

/// @defgroup CasacoreDataTypes Common casacore data types aliases
/// @{
using rownr_t = casacore::rownr_t;
using Bool = casacore::Bool;
using uChar = casacore::uChar;
using Short = casacore::Short;
using uShort = casacore::uShort;
using Int = casacore::Int;
using uInt = casacore::uInt;
using Int64 = casacore::Int64;
using Float = casacore::Float;
using Double = casacore::Double;
using Complex = casacore::Complex;
using DComplex = casacore::DComplex;
using String = casacore::String;
/// @}

/// @defgroup CasacoreClasses Common casacore classes aliases
/// @{
using AipsIO = casacore::AipsIO;
using ArrayBase = casacore::ArrayBase;
using DataManager = casacore::DataManager;
using DataManagerColumn = casacore::DataManagerColumn;
using IPosition = casacore::IPosition;
using Record = casacore::Record;
using RefRows = casacore::RefRows;
using Slicer = casacore::Slicer;
/// @}

} // namespace plasma
} // namespace ska

#endif // SKA_PLASMA_CASACORE_TYPES_H