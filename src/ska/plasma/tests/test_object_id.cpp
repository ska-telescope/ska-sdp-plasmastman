#include <gtest/gtest.h>

#include "ska/plasma/exceptions.h"
#include "ska/plasma/object_id.h"
#include "test_common.h"

namespace ska {
namespace plasma {

TEST(ObjectIdTests, ValidObjectIDs)
{
	for (auto valid_id : {VALID_OBJECT_ID1, VALID_OBJECT_ID2})
	{
		ObjectID id{valid_id};
		ASSERT_TRUE(id.valid());
	}
}

TEST(ObjectIdTests, InvalidObjectIDs)
{
	for (auto invalid_id : {INVALID_OBJECT_ID1, INVALID_OBJECT_ID2, INVALID_OBJECT_ID3})
	{
		EXPECT_THROW(
			ObjectID{invalid_id},
			invalid_object_id
		);
	}
}

TEST(ObjectIdTests, EmptyObjectID)
{
	ObjectID id;
	ASSERT_FALSE(id.valid());
}

}  // namespace plasma
}  // namespace ska

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}