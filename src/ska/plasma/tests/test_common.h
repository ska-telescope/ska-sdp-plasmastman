#ifndef SRC_SKA_PLASMA_TESTS_TEST_COMMON_H
#define SRC_SKA_PLASMA_TESTS_TEST_COMMON_H

#include <random>
#include <string>
#include <vector>

#include <arrow/ipc/writer.h>
#include <arrow/io/memory.h>
#include <arrow/tensor.h>
#include <arrow/type.h>
#include <plasma/client.h>
#include <plasma/common.h>

#include "ska/plasma/types.h"

namespace ska {
namespace plasma {

constexpr const char *VALID_OBJECT_ID1   = "00000000000000000000";
constexpr const char *VALID_OBJECT_ID2   = "00000000000000000001";
constexpr const char *INVALID_OBJECT_ID1 = "00000000000000000001--";
constexpr const char *INVALID_OBJECT_ID2 = "0000000000000000--";
constexpr const char *INVALID_OBJECT_ID3 = "";

#define FOR_ALL_TYPES_EXCEPT_COMPLEX(f) \
    f<uChar>();          \
    f<Short>();          \
    f<uShort>();         \
    f<Int>();            \
    f<uInt>();           \
    f<Int64>();          \
    f<Float>();          \
    f<Double>();         \

#define FOR_COMPLEX_TYPES(f) \
    f<Complex>();            \
    f<DComplex>()            \

#define FOR_ALL_TYPES(f)             \
    FOR_ALL_TYPES_EXCEPT_COMPLEX(f)  \
    FOR_COMPLEX_TYPES(f)             \

template <typename T>
T non_zero_value()
{
	return T{123};
}

template <>
std::complex<float> non_zero_value<>()
{
	return std::complex<float>{123, 456};
}

template <>
std::complex<double> non_zero_value<>()
{
	return std::complex<double>{123, 456};
}

template <typename Func, typename ... Args>
void arrow_safe(Func &&f, Args && ... args)
{
	if (!f(std::forward<Args>(args)...).ok())
	{
		throw std::runtime_error("error");
	}
}

::plasma::ObjectID get_random_object_id()
{
	// Random 20-byte object ID
	std::string _object_id(20, '0');
	std::default_random_engine rng(std::random_device{}());
	std::uniform_int_distribution<unsigned char> dist;
	std::generate_n(_object_id.begin(), 20, [&](){ return dist(rng); });
	return ::plasma::ObjectID::from_binary(_object_id);
}

::plasma::PlasmaClient get_plasma_client()
{
	::plasma::PlasmaClient client;
	if (!client.Connect(std::getenv("PLASMA_SOCKET")).ok())
	{
		throw std::runtime_error("connect error");
	}
	return client;
}

template <typename T>
std::tuple<std::vector<T>, std::size_t> prepare_data(std::size_t nrows,
  T expected_datum, const casacore::IPosition &cell_shape = {1})
{
	// Create a 1D tensor with the expected value situated at the given row,
	// the rest are default-created
	auto nvalues = nrows * cell_shape.product();
	std::vector<T> values(nvalues);
	std::default_random_engine rng(std::random_device{}());
	auto row = std::uniform_int_distribution<std::size_t>(0, nrows - 1)(rng);
	values[row * cell_shape.product()] = expected_datum;

	return std::make_tuple(std::move(values), row);
}

template <typename T>
std::unique_ptr<arrow::Tensor> to_tensor(const std::vector<T> &values, const std::vector<std::int64_t> &tensor_shape)
{
	// Vector to tensor
	const uint8_t* bytes_array = reinterpret_cast<const uint8_t*>(values.data());
	auto value_buffer = std::make_shared<arrow::Buffer>(bytes_array, sizeof(T) * values.size());
	return std::make_unique<arrow::Tensor>(get_tensor_type<T>(),
	  value_buffer, tensor_shape);
}

::plasma::ObjectID write_to_plasma(const arrow::Tensor &tensor)
{
	auto client = get_plasma_client();
	auto object_id = get_random_object_id();

	int64_t datasize;
	arrow_safe(arrow::ipc::GetTensorSize, tensor, &datasize);
	std::shared_ptr<Buffer> buffer;
	if (!client.Create(object_id, datasize, NULL, 0, &buffer).ok())
	{
		throw std::runtime_error("create error");
	}
	arrow::io::FixedSizeBufferWriter stream(buffer);
	int32_t meta_len = 0;
	arrow_safe(arrow::ipc::WriteTensor, tensor, &stream, &meta_len, &datasize);
	if (!client.Seal(object_id).ok())
	{
		throw std::runtime_error("seal error");
	}
	return object_id;
}

}  // namespace plasma
}  // namespace ska

#endif /* SRC_SKA_PLASMA_TESTS_TEST_COMMON_H */