#ifndef SRC_SKA_PLASMA_TESTS_COMMON_ASSERTIONS_H
#define SRC_SKA_PLASMA_TESTS_COMMON_ASSERTIONS_H

#include <vector>

#include <gtest/gtest.h>

#include "ska/plasma/plasma_stman.h"
#include "ska/plasma/casacore_types.h"
#include "test_common.h"


namespace ska {
namespace plasma {

template <typename T>
void assert_scalar_cell(DataManagerColumn *column, rownr_t nrows,
    const std::vector<T> &values, std::size_t row)
{
	// Read with the storage manager column. `row` must have the given value,
	// others are default
	T datum;
	column->get(row, &datum);
	EXPECT_EQ(datum, non_zero_value<T>());
	if (row < nrows - 1)
	{
		column->get(row + 1, &datum);
		EXPECT_EQ(datum, T{});
	}
	if (row > 1)
	{
		column->get(row - 1, &datum);
		EXPECT_EQ(datum, T{});
	}
}

template <typename T>
void assert_scalar_column(DataManagerColumn *column, rownr_t nrows,
    const std::vector<T> &values, std::size_t /*row*/)
{
	// The full column must be the same as the values that were written via
	// plasma
	casacore::Vector<T> column_data(nrows);
	column->getScalarColumnV(column_data);
	EXPECT_EQ(values, column_data.tovector());
}

template <typename T>
void assert_scalar_continuous_column_cells(DataManagerColumn *column,
    rownr_t nrows, const std::vector<T> &values, std::size_t row)
{
	// A single, continuous  row slice with up to 3 rows
	casacore::rownr_t start = row - (row >= 1);
	casacore::rownr_t end = row + (row < nrows - 1);
	auto slice_size = end - start + 1;

	casacore::Vector<T> column_data(slice_size);
	column->getScalarColumnCellsV(casacore::RefRows(start, end), column_data);

	std::vector<T> expected_slice (slice_size);
	std::copy(values.begin() + start, values.begin() + start + slice_size, expected_slice.begin());
	EXPECT_EQ(column_data.size(), slice_size);
	EXPECT_EQ(column_data.tovector(), expected_slice);
}

template <typename T>
void assert_scalar_disjoint_column_cells(DataManagerColumn *column,
    rownr_t nrows, const std::vector<T> &values, std::size_t row)
{
	// Three disjoint rows, should use default row-by-row behavior
	casacore::rownr_t first = row >= 2 ? row - 2 : 2;
	casacore::rownr_t second = row;
	casacore::rownr_t third = row < nrows - 2 ? row + 2 : nrows - 2;

	casacore::Vector<casacore::rownr_t> rows {first, second, third};
	casacore::Vector<T> column_data(3);
	column->getScalarColumnCellsV(casacore::RefRows{rows}, column_data);

	std::vector<T> expected_slice(3);
	expected_slice[0] = values[first];
	expected_slice[1] = values[second];
	expected_slice[2] = values[third];
	EXPECT_EQ(column_data.size(), 3);
	EXPECT_EQ(column_data.tovector(), expected_slice);
}

}  // namespace plasma
}  // namespace ska

#endif /* SRC_SKA_PLASMA_TESTS_COMMON_ASSERTIONS_H */
