#include <algorithm>
#include <complex>
#include <utility>
#include <vector>

#include <gtest/gtest.h>
#include <casacore/casa/Arrays/Vector.h>
#include <casacore/tables/Tables/RefRows.h>

#include "common_assertions.h"
#include "ska/plasma/plasma_stman.h"
#include "test_common.h"

namespace ska {
namespace plasma {

template <typename T>
std::vector<std::int64_t> get_scalar_tensor_shape(const std::vector<T> &values)
{
	return {static_cast<std::int64_t>(values.size())};
}

template <typename T>
std::vector<std::int64_t> get_scalar_tensor_shape(const std::vector<std::complex<T>> &values)
{
	auto size = static_cast<std::int64_t>(values.size());
	return {size, 2};
}

template <typename T, typename CheckingRoutine>
static void read_and_check(T expected_datum, CheckingRoutine && check_routine)
{
	std::size_t nrows = 256;
	std::vector<T> values;
	std::size_t row;
	std::tie(values, row) = prepare_data(nrows, expected_datum);

	auto tensor = to_tensor(values, get_scalar_tensor_shape(values));
	auto object_id = write_to_plasma(*tensor);

	// Create a storage manager and column like a user would using a Table.
	// This is enough to pass down all the information we need
	PlasmaStMan stman("", {{"DATA", object_id.binary()}});
	stman.create64(nrows);
	auto column = stman.makeScalarColumn("DATA", casacore_type_v<T>, "dataTypeID");

	check_routine(column, nrows, values, row);
}

template <typename T>
static void read_cell()
{
	T expected_datum = non_zero_value<T>();
	read_and_check(expected_datum, assert_scalar_cell<T>);
}

template <typename T>
static void read_column()
{
	T expected_datum = non_zero_value<T>();
	read_and_check(expected_datum, assert_scalar_column<T>);
}

template <typename T>
static void read_column_cells_continuous()
{
	T expected_datum = non_zero_value<T>();
	read_and_check(expected_datum, assert_scalar_continuous_column_cells<T>);
}

template <typename T>
static void read_column_cells_disjoint()
{
	T expected_datum = non_zero_value<T>();
	read_and_check(expected_datum, assert_scalar_disjoint_column_cells<T>);
}


TEST(PlasmaStManScalarReadTests, ReadSingleCell)
{
	FOR_ALL_TYPES(read_cell);
}

TEST(PlasmaStManScalarReadTests, ReadColumn)
{
	FOR_ALL_TYPES(read_column);
}

TEST(PlasmaStManScalarReadTests, ReadColumnCellsContinuous)
{
	FOR_ALL_TYPES(read_column_cells_continuous);
}

TEST(PlasmaStManScalarReadTests, ReadColumnCellsDisjoint)
{
	FOR_ALL_TYPES(read_column_cells_disjoint);
}

} // namespace plasma
} // namespace ska

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}