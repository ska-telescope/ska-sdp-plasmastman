#ifndef SKA_PLASMA_OBJECT_ID_H
#define SKA_PLASMA_OBJECT_ID_H

#include <string>

namespace ska {
namespace plasma {

/**
 * Simple, immutable class containing an Object ID.
 *
 * This is a simpler version of plasma's own Object ID class, but without
 * carrying all its dependencies, allowing us to have a specific type to
 * represent Object IDs (other than std::string) without permeating the codebase
 * with plasma dependencies.
 */
class ObjectID
{
private:
	/// The actual ID
	std::string _object_id;

public:
	/// Construct an empty ObjectID, it can't be used for anything.
	ObjectID() = default;

	/**
	 * Constructs an Object ID for the given string, which must be a valid
	 * plasma Object ID.
	 * @param object_id The contents of the Object ID
	 */
	ObjectID(const std::string &object_id);

	/**
	 * Constructs an Object ID for the given null-terminated C string, which
	 * must be a valid plasma Object ID.
	 * @param object_id The contents of the Object ID
	 */
	ObjectID(const char *object_id);

	/**
	 * Returns the underlying string.
	 * @return The underlying string
	 */
	const std::string &string() const
	{
		return _object_id;
	}

	/**
	 * Returns whether this is a valid Object ID or not.
	 * @return true if this Object ID is valid
	 */
	bool valid() const
	{
		return !_object_id.empty();
	}
};

}  // namespace plasma
}  // namespace ska

#endif // SKA_PLASMA_OBJECT_ID_H