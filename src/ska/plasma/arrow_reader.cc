#include <casacore/casa/BasicSL/STLIO.h>

#include "ska/plasma/arrow_reader.h"
#include "ska/plasma/exceptions.h"

namespace ska {
namespace plasma {

void ArrowReader::check_expected(const arrow::DataType &dt, const arrow::DataType &expected) const
{
	if (!dt.Equals(expected))
	{
		std::ostringstream os;
		os << "Arrow type in " << column_name() << " different from expected: "
		   << dt.ToString() << " != " << expected.ToString();
		throw type_error(os.str());
	}
}

void ArrowReader::check_expected(const Shape &shape, const Shape &expected) const
{
	if (shape != expected)
	{
		std::ostringstream os;
		os << "Column shape different from Arrow shape: ";
		casacore::operator<<(os, expected) << " != ";
		casacore::operator<<(os, shape);
		throw type_error(os.str());
	}
}

}  // namespace plasma
}  // namespace ska