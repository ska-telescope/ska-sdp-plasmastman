#ifndef SRC_SKA_PLASMA_TENSOR_READER_H_
#define SRC_SKA_PLASMA_TENSOR_READER_H_

#include <arrow/type.h>
#include <arrow/io/interfaces.h>

#include "ska/plasma/arrow_reader.h"

namespace ska {
namespace plasma {

/**
 * An ArrowReader that reads data off an Arrow Tensor.
 *
 * TODO: The current implementation contains two private templated methods to
 * handle all data types. This means we need to continuously do a runtime check
 * for the casacore data type to choose the correct template instance. This
 * could be avoided by offering a TensorReaderBase class that handles all common
 * aspects, then a TensorReader class templated on the casacore data type, and
 * finally a factory function that is called once from PlasmaStManColumn to
 * create the correct reader for the given casacore data type.
 */
class TensorReader : public ArrowReader
{
private:
	/// The tensor read by this reader
	std::shared_ptr<arrow::Tensor> _tensor;

	/// @see read_scalar
	template <typename CasacoreType>
	void read_scalar_impl(rownr_t rownr, void *dataPtr);

	/// @see read_array
	template <typename CasacoreType>
	void read_array_impl(ArrayBase &array, std::size_t offset);

protected:
	void check_shape(const Shape &column_shape) const override;
	void check_types() const override final;

public:
	/**
	 * Constructs a TensorReader for the given casacore data type and column
	 * from an input stream.
	 * @param column_name The casacore column backed by this reader.
	 * @param data_type The casacore data type of the column backed by this
	 * @param input_stream The input stream from where the Tensor will be read.
	 * This is possibly created from an object read from Plasma.
	 */
	TensorReader(const std::string &column_name, casacore::DataType data_type,
	  arrow::io::InputStream* input_stream);

	void read_scalar(rownr_t rownr, void *dataPtr) override;
	void read_array(ArrayBase &array, std::size_t offset) override;
};

}  // namespace plasma
}  // namespace ska

#endif /* SRC_SKA_PLASMA_TENSOR_READER_H_ */