#ifndef SKA_PLASMA_EXCEPTIONS_H
#define SKA_PLASMA_EXCEPTIONS_H

#include <exception>
#include <string>

namespace ska {
namespace plasma {

/// Base exception for Plasma storage manager errors
class exception : public std::exception
{

private:
	std::string _what;

public:

	/**
	 * Create an exception
	 *
	 * @param what The error message
	 */
	explicit exception(const std::string what)
	  : _what(what)
	{
	}

	/// Destructor
	~exception() noexcept = default;

	/// @see std::exception::what
	const char *what() const noexcept override
	{
		return _what.c_str();
	}

};


/// Error thrown by unimplemented methods/functions
class unimplemented_error : public exception
{
public:
	/**
	 * Create a new unimplemnted_error for the given function name, file and
	 * line number.
	 * @param function The function that is currently unimplemented.
	 * @param file The file where the function resides.
	 * @param line The line where the implementation is missing.
	 */
	explicit unimplemented_error(const std::string function,
	  const std::string &file, int line)
	  : exception(file + ":" + std::to_string(line) + " " + function +
		          " is unimplemented")
	{
	}
};

/// Creates an unimplemented_error with the correct context information
#define UNIMPLEMENTED() unimplemented_error(static_cast<const char *>(__PRETTY_FUNCTION__), __FILE__, __LINE__)


/// Error thrown by plasma-related problems
class plasma_error : public exception
{
public:
	/**
	 * Create a new plasma_error instance with a given error message.
	 * @param what The error message.
	 */
	plasma_error(const std::string &what)
	  : exception(what)
	{
	}
};

/// Error thrown by arrow type-related problems
class type_error : public exception
{
public:
	/**
	 * Creates a new type_error instance with a given error message.
	 * @param what The error message.
	 */
	type_error(const std::string &what)
	  : exception(what)
	{
	}
};

/// Error thrown when no mapping is found for a given column
class no_column_mapping_error : public exception
{
public:
	/**
	 * Create an instance of this class for the given column name
	 * @param column The column name
	 */
	no_column_mapping_error(const std::string &column)
	  : exception(std::string("No mapping found for column : ") + column)
	{
	}
};

/// Error thrown when an Object ID is invalid
class invalid_object_id : public exception
{
public:
	/**
	 * Create an instance of this class for the given invalid Object ID
	 * @param object_id An invalid Object ID
	 */
	invalid_object_id(const std::string &object_id);
};

} // namespace plasma
} // namespace ska

#endif // SKA_PLASMA_EXCEPTIONS_H