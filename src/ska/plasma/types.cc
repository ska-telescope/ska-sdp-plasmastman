#include <map>

#include "ska/plasma/exceptions.h"
#include "ska/plasma/types.h"

namespace ska {
namespace plasma {

using TypeMap = std::map<casacore::DataType, std::shared_ptr<arrow::DataType>>;

static const TypeMap simple_arrow_types =
{
	{casacore::DataType::TpUChar,    arrow::uint8()},
	{casacore::DataType::TpShort,    arrow::int16()},
	{casacore::DataType::TpUShort,   arrow::uint16()},
	{casacore::DataType::TpInt,      arrow::int32()},
	{casacore::DataType::TpUInt,     arrow::uint32()},
	{casacore::DataType::TpInt64,    arrow::int64()},
	{casacore::DataType::TpFloat,    arrow::float32()},
	{casacore::DataType::TpDouble,   arrow::float64()},
};
static const TypeMap tensor_extra_types =
{
	{casacore::DataType::TpBool,     arrow::uint8()},
	{casacore::DataType::TpComplex,  arrow::float32()},
	{casacore::DataType::TpDComplex, arrow::float64()}
};
static const TypeMap table_extra_types =
{
	{casacore::DataType::TpBool,     arrow::boolean()},
	{casacore::DataType::TpComplex,  arrow::struct_(
		{
			arrow::field("r", arrow::float32()),
			arrow::field("i", arrow::float32()),
		}
	)},
	{casacore::DataType::TpDComplex, arrow::struct_(
		{
			arrow::field("r", arrow::float64()),
			arrow::field("i", arrow::float64()),
		}
	)}
};

static std::shared_ptr<arrow::DataType> _get_type(casacore::DataType dtype,
  const TypeMap &extra_types)
{
	try
	{
		return extra_types.at(dtype);
	}
	catch (const std::out_of_range &)
	{
		try
		{
			return simple_arrow_types.at(dtype);
		}
		catch (const std::out_of_range &)
		{
			std::ostringstream os;
			os << "Unsupported type: " << dtype;
			throw type_error(os.str());
		}
	}
}

std::shared_ptr<arrow::DataType> get_tensor_type(casacore::DataType dtype)
{
	return _get_type(dtype, tensor_extra_types);
}

std::shared_ptr<arrow::DataType> get_table_type(casacore::DataType dtype)
{
	return _get_type(dtype, table_extra_types);
}

}  // namespace plasma
}  // namespace ska