#ifndef SRC_SKA_PLASMA_TYPES_H
#define SRC_SKA_PLASMA_TYPES_H

#include <arrow/type.h>
#include <casacore/casa/Utilities/DataType.h>

#include "ska/plasma/casacore_types.h"

namespace ska {
namespace plasma {

/**
 * Type trait for translation between data types and casacore::DataType
 * enumeration values at compile-time.
 */
template <typename>
struct casacore_type;

/**
 * @defgroup TypeTraitsSpeicializations Specializations for type_traits
 * @{
 */
template <>
struct casacore_type<Bool>
{
	constexpr static casacore::DataType value = casacore::DataType::TpBool;
};

template <>
struct casacore_type<uChar>
{
	constexpr static casacore::DataType value = casacore::DataType::TpUChar;
};

template <>
struct casacore_type<Short>
{
	constexpr static casacore::DataType value = casacore::DataType::TpShort;
};

template <>
struct casacore_type<uShort>
{
	constexpr static casacore::DataType value = casacore::DataType::TpUShort;
};

template <>
struct casacore_type<Int>
{
	constexpr static casacore::DataType value = casacore::DataType::TpInt;
};

template <>
struct casacore_type<uInt>
{
	constexpr static casacore::DataType value = casacore::DataType::TpUInt;
};

template <>
struct casacore_type<Int64>
{
	constexpr static casacore::DataType value = casacore::DataType::TpInt64;
};

template <>
struct casacore_type<Float>
{
	constexpr static casacore::DataType value = casacore::DataType::TpFloat;
};

template <>
struct casacore_type<Double>
{
	constexpr static casacore::DataType value = casacore::DataType::TpDouble;
};

template <>
struct casacore_type<Complex>
{
	constexpr static casacore::DataType value = casacore::DataType::TpComplex;
};

template <>
struct casacore_type<DComplex>
{
	constexpr static casacore::DataType value = casacore::DataType::TpDComplex;
};
/// @}

//// Helper variable template for the casacore_type trait.
template <typename T>
constexpr casacore::DataType casacore_type_v = casacore_type<T>::value;

/**
 * Returns the Tensor Arrow data type corresponding to the given casacore data type
 * @param dtype A casacore DataType identifier
 * @return The expected Tensor Arrow data type
 */
std::shared_ptr<arrow::DataType> get_tensor_type(casacore::DataType dtype);

/**
 * Like get_tensor_type(casacore::DataType), but templated on a type.
 * @tparam T The compile-time type
 * @return The expected Tensor Arrow data type
 * @see get_tensor_type(casacore::DataType)
 */
template <typename T>
std::shared_ptr<arrow::DataType> get_tensor_type()
{
	return get_tensor_type(casacore_type_v<T>);
}

/**
 * Returns the Table Arrow data type corresponding to the given casacore data type
 * @param dtype A casacore DataType identifier
 * @return The expected Table arrow data type
 */
std::shared_ptr<arrow::DataType> get_table_type(casacore::DataType dtype);

/**
 * Like get_table_type(casacore::DataType), but templated on a type.
 * @tparam T The compile-time type
 * @return The expected Tensor Arrow data type
 * @see get_table_type(casacore::DataType)
 */
template <typename T>
std::shared_ptr<arrow::DataType> get_table_type()
{
	return get_table_type(casacore_type_v<T>);
}

/**
 * Invokes the given function template @p F with the correct template type
 * depending on the value of @p dtype.
 * @param dtype A casacore::DataType data type.
 * @param F A function template.
 * @param ... The parameters to call F with.
 */
#define INVOKE_FOR_DATATYPE(dtype, F, ...)        \
{                                                 \
    using DataType = casacore::DataType;          \
    switch (dtype) {                              \
    case DataType::TpBool:                        \
        F<Bool>(__VA_ARGS__);                     \
        break;                                    \
    case DataType::TpUChar:                       \
        F<uChar>(__VA_ARGS__);                    \
        break;                                    \
    case DataType::TpShort:                       \
        F<Short>(__VA_ARGS__);                    \
        break;                                    \
    case DataType::TpUShort:                      \
        F<uShort>(__VA_ARGS__);                   \
        break;                                    \
    case DataType::TpInt:                         \
        F<Int>(__VA_ARGS__);                      \
        break;                                    \
    case DataType::TpUInt:                        \
        F<uInt>(__VA_ARGS__);                     \
        break;                                    \
    case DataType::TpInt64:                       \
        F<Int64>(__VA_ARGS__);                    \
        break;                                    \
    case DataType::TpFloat:                       \
        F<Float>(__VA_ARGS__);                    \
        break;                                    \
    case DataType::TpDouble:                      \
        F<Double>(__VA_ARGS__);                   \
        break;                                    \
    case DataType::TpComplex:                     \
        F<Complex>(__VA_ARGS__);                  \
        break;                                    \
    case DataType::TpDComplex:                    \
        F<DComplex>(__VA_ARGS__);                 \
        break;                                    \
    default:                                      \
        throw exception(                          \
          std::string("Unsupported data type: ")  \
          + std::to_string(dtype));               \
    }                                             \
}

}  // namespace plasma
}  // namespace ska


#endif /* SRC_SKA_PLASMA_TYPES_H */