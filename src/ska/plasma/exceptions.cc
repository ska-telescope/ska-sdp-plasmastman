#include <iomanip>
#include <iterator>

#include "ska/plasma/exceptions.h"

namespace ska {
namespace plasma {

static std::string to_hex(const std::string &object_id)
{
	std::ostringstream os;
	os << std::hex << std::setw(2);
	std::copy(object_id.begin(), object_id.end(), std::ostream_iterator<int>(os));
	return os.str();
}

invalid_object_id::invalid_object_id(const std::string &object_id)
  : exception(std::string("Invalid Object ID : hex=") +
	          to_hex(object_id) + ", str=\"" + object_id + '"')
{
}

} // namespace plasma
} // namespace ska