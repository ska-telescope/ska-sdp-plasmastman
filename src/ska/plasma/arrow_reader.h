#ifndef SKA_PLASMA_ARROW_READER_H
#define SKA_PLASMA_ARROW_READER_H

#include <memory>
#include <cstdint>
#include <string>
#include <vector>

#include <arrow/type.h>
#include <casacore/casa/Utilities/DataType.h>

#include "ska/plasma/casacore_types.h"
#include "ska/plasma/shape.h"

namespace ska {
namespace plasma {

/**
 * Base class for Arrow data readers used by the PlasmaStManColumn class.
 *
 * Arrow offers different storage types, like Tensors and Tables. This base
 * class offers a common interface for accessing data from these different
 * storage types.
 */
class ArrowReader
{
private:
	/// The name of the column backed by this reader.
	std::string _column_name;
	/// The casacore data type of the column backed by this reader.
	casacore::DataType _casacore_type;

protected:
	/**
	 * @return The casacore data type of the column backed by this reader.
	 */
	casacore::DataType casacore_type() const
	{
		return _casacore_type;
	}

	/**
	 * @return The name of the column backed by this reader.
	 */
	std::string column_name() const
	{
		return _column_name;
	}

	/**
	 * Checks that the shape of the underlying Arrow object conforms to the
	 * shape of the casacore column backed by this reader.
	 * @param column_shape The shape of the casacore column.
	 */
	virtual void check_shape(const Shape &column_shape) const = 0;

	/**
	 * Checks that the casacore data type of column backed by this reader, and
	 * the data type of the Arrow object underlying this reader are compatible.
	 * It is called at least once after construction when the column name is set
	 * on the reader object; this is because some readers might require the
	 * column name to correctly function.
	 */
	virtual void check_types() const = 0;

	/**
	 * Checks that the given data type is the same as the expected one. If they
	 * are not an exception is thrown.
	 * @param dt The Arrow data type to check.
	 * @param expected The expected Arrow data type.
	 */
	void check_expected(const arrow::DataType &dt, const arrow::DataType &expected) const;

	/**
	 * Checks that the given shape is the same as the expected one, which is
	 * that of the underlying casacore column. If they are not an exception is
	 * thrown.
	 * @param shape The shape to check.
	 * @param expected The expected Arrow shape.
	 */
	void check_expected(const Shape &shape, const Shape &expected) const;

public:
	/**
	 * Constructs a reader for the given data type.
	 * @param column_name The casacore column backed by this reader.
	 * @param data_type The casacore data type of the column backed by this
	 * reader.
	 */
	ArrowReader(const std::string &column_name, casacore::DataType data_type)
	 : _column_name(column_name), _casacore_type(data_type)
	{
	}

	/// Virtual destructor required by virtual base class.
	virtual ~ArrowReader() = default;

	/**
	 * Checks that the data type and the shape of the underlying Arrow object
	 * match those of the casacore column this reader backs up. The column data
	 * type is known at construction time, and the column shape is given here.
	 * @param column_shape The shape of the casacore column this reader backs up.
	 */
	void check_conformance(const Shape &column_shape)
	{
		check_shape(column_shape);
		check_types();
	}

	/**
	 * Read a single scalar value from the underlying Arrow object. The scalar
	 * value is that corresponding to the cell in row @a rownr.
	 * @param rownr The (casacore) row number of the cell for which the scalar
	 *  is being read.
	 * @param dataPtr The address where the scalar should be written to.
	 */
	virtual void read_scalar(rownr_t rownr, void *dataPtr) = 0;

	/**
	 * Read an array from the underlying Arrow object starting at the given
	 * offset. The array's shape determines how much data is effectively read,
	 * and might or might not be able to be created with zero-copy.
	 * @param array The array where the data should be read into.
	 * @param offset The offset in the underlying Arrow object at which reading
	 * will start.
	 */
	virtual void read_array(ArrayBase &array, std::size_t offset) = 0;
};

}  // namespace plasma
}  // namespace ska

#endif // SKA_PLASMA_ARROW_READER_H