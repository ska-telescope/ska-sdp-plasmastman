#include "ska/plasma/exceptions.h"
#include "ska/plasma/object_id.h"

namespace ska {
namespace plasma {

static bool is_valid_object_id(const std::string &object_id)
{
	return object_id.size() == 20;
}

ObjectID::ObjectID(const std::string &object_id)
{
	if (!is_valid_object_id(object_id))
	{
		throw invalid_object_id(object_id);
	}
	_object_id = object_id;
}

ObjectID::ObjectID(const char *object_id)
  : ObjectID(std::string(object_id))
{
}

}  // namespace plasma
}  // namespace ska