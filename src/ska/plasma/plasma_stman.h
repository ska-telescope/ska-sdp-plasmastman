#ifndef SKA_PLASMA_PLASMA_STMAN_H
#define SKA_PLASMA_PLASMA_STMAN_H

#include <map>
#include <memory>
#include <string>

#include <casacore/tables/DataMan/DataManager.h>

#include "ska/plasma/object_id.h"

namespace ska {
namespace plasma {

/**
 * The Plasma-based storage manager
 *
 * This is implemented using a pimpl idiom to hide the particulars of the
 * implementation and hide it from users.
 */
class PlasmaStMan : public casacore::DataManager {

private:
	friend class PlasmaStManColumn;
	class impl;
	std::unique_ptr<impl> pimpl;

public:
	/**
	 * Creates a new instance of the Plasma Storage Manager connected to the
	 * given socket, and mapping columns to Arrow Tensors and Tables as
	 * indicated in the given mappings.
	 * @param plasma_socket The UNIX socket where the Plasma store listens for
	 * connections. If not given, or empty, it defaults to @p /tmp/plasma,
	 * unless the @p PLASMA_SOCKET environment variable is set, in which case
	 * its value takes precedence.
	 * @param tensor_object_ids A mapping from column names to Object IDs in the
	 * Plasma store where Arrow Tensors with the data for the respective column
	 * can be found.
	 * @param table_object_ids A mapping from column names to Object IDs in the
	 * Plasma store where Arrow Tables with the data for the respective column
	 * can be found (the name of the column being mapped must be the same as the
	 * column name in the Arrow Table).
	 */
	PlasmaStMan(std::string plasma_socket = "",
	  const std::map<std::string, ObjectID> &tensor_object_ids = {},
	  const std::map<std::string, ObjectID> &table_object_ids = {});

	/**
	 * Destructor declaration because of the pimpl idiom, otherwise its
	 * implementation is defaulted.
	 */
	~PlasmaStMan();

	/// @see PlasmaClient::ping
	void ping_plasma();

	/// @see PlasmaClient::set_get_timeout
	void set_plasma_get_timeout(std::int64_t timeout);

	/// @see PlasmaClient::set_connect_retries
	void set_plasma_connect_retries(int connect_retries);

	casacore::DataManager *clone() const override;
	casacore::String dataManagerType() const override;
	casacore::String dataManagerName() const override;
	void create64(casacore::rownr_t aNrRows) override;
	casacore::rownr_t open64(casacore::rownr_t aRowNr, casacore::AipsIO &ios) override;
	casacore::rownr_t resync64(casacore::rownr_t aRowNr) override;
	casacore::Bool flush(casacore::AipsIO &, casacore::Bool doFsync) override;
	casacore::DataManagerColumn *makeScalarColumn(const casacore::String &aName,
	  int aDataType, const casacore::String &aDataTypeID) override;
	casacore::DataManagerColumn *makeDirArrColumn(const casacore::String &aName,
	  int aDataType, const casacore::String &aDataTypeID) override;
	casacore::DataManagerColumn *makeIndArrColumn(const casacore::String &aName,
	  int aDataType, const casacore::String &aDataTypeID) override;
	void deleteManager() override;
	void addRow64(casacore::rownr_t aNrRows) override;
	casacore::Record dataManagerSpec() const override;
	casacore::Record getProperties() const override;
	void setProperties (const casacore::Record& props) override;

	/**
	 * Factory function invoked by casacore to create an instance of PlasmaStMan
	 * from a given DataManager specification.
	 * @param aDataManType The name of the data manager.
	 * @param spec The specification of the data manager.
	 * @return A new PlasmaStMan object.
	 * @see PlasmaStMan::impl::dataManagerSpec
	 */
	static casacore::DataManager *makeObject(
	  const casacore::String &aDataManType, const casacore::Record &spec);

};

/**
 * Entry point for the casacore library loading mechanism to register the
 * PlasmaStMan factory function.
 * @see PlasmaStMan::makeObject
 */
extern "C" void register_plasmastman();

} // namespace plasma
} // namespace ska

#endif // SKA_PLASMA_PLASMA_STMAN_H