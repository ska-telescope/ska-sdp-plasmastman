import argparse
import collections
import os
import random
import signal
import string
import subprocess
import sys

import numpy as np
import pyarrow
import pyarrow.plasma as plasma
import functools
import contextlib


class Store(object):
    """A context manager for a plasma store process"""

    def __init__(self, socket):
        self.socket = socket
        self.store = None
    def __enter__(self):
        self.store = subprocess.Popen(['plasma_store', '-s', self.socket, '-m', '10000'])
    def __exit__(self, *args):
        if self.store:
            self.store.terminate()
            self.store.wait()

def wait_forever():
    try:
        signal.pause()
    except KeyboardInterrupt:
        pass

def _plasma_socket(s):
    if not s:
        s = os.environ.get('PLASMA_SOCKET', '/tmp/plasma')
    return s


ColumnInfo = collections.namedtuple('ColumnInfo', 'name dtype cell_shape values')
def column_info(s):
    column_name, s = s.split('/')
    typespec, values = s.split('=')
    values = values.split(',')

    is_array = ':' in typespec
    if is_array:
        dtype, *cell_shape = typespec.split(':')
        cell_shape = tuple(map(int, cell_shape))
        shape = len(values) // functools.reduce(lambda x, y: x * y, cell_shape), *cell_shape
    else:
        dtype = typespec
        cell_shape = None
        shape = len(values),

    # Get shape and dtype for numpy array construction
    shape = list(shape)
    np_dtype = dtype
    if dtype in ('complex', 'dcomplex'):
        np_dtype = 'float32' if dtype == 'complex' else 'float64'
        shape[0] //= 2
        shape.append(2)
    values = np.array(values, dtype=np_dtype)
    values = values.reshape(shape)

    return ColumnInfo(column_name, dtype, cell_shape, values)

TensorInfo = collections.namedtuple('TensorInfo', 'column_name dtype cell_shape object_id tensor')
def tensor_info(s):
    col = column_info(s)
    object_id = ''.join(random.choice(string.digits) for _ in range(20))
    return TensorInfo(
        col.name, col.dtype, col.cell_shape, object_id,
        pyarrow.Tensor.from_numpy(col.values)
    )

TableInfo = collections.namedtuple('TableInfo', 'columns object_id table')
def table_info(s):
    table = {}
    columns = []
    for col in s.split('|'):
        col = column_info(col)
        values = col.values
        if col.dtype in ('complex', 'dcomplex'):
            array = pyarrow.StructArray.from_arrays([
                values[:,0], values[:,1]
            ], names=['r', 'i'])
        else:
            array = pyarrow.array(values)
        table[col.name] = array
        columns.append(col)
    object_id = ''.join(random.choice(string.digits) for _ in range(20))
    return TableInfo(columns, object_id, pyarrow.table(table))


_NPY_DTYPE_TO_TAQL_TYPE = {
    'bool': 'bool',
    'int8': 'char',
    'uint8': 'uchar',
    'int16': 'short',
    'uint16': 'ushort',
    'int32': 'int',
    'uint32': 'uint',
    'int64': 'long',
    'float32': 'float',
    'float64': 'double',
    'complex': 'complex',
    'dcomplex': 'dcomplex'
}

def write_table(table, rows, tensors, tables):

    column_names = ('[' + ','.join(
        [f'"{t.column_name}"' for t in tensors] +
        [f'"{c.name}"' for t in tables for c in t.columns]
    ) + ']')
    dmspec = []
    if tensors:
        dmspec.append('TENSOROBJECTIDS=[' + ','.join(f'{t.column_name}="{t.object_id}"' for t in tensors) + ']')
    if tables:
        dmspec.append('TABLEOBJECTIDS=[' + ','.join(f'{column.name}="{t.object_id}"' for t in tables for column in t.columns) + ']')
    dmspec = '[' + ','.join(spec for spec in dmspec) + ']'
    dminfo = f'[NAME="CommonPlasmaStMan",TYPE="PlasmaStMan",SPEC={dmspec},COLUMNS={column_names}]'

    def get_colspec(column_name, column):
        spec = f'{column_name} {_NPY_DTYPE_TO_TAQL_TYPE[column.dtype]}'
        if column.cell_shape:
            spec += ' SHAPE=[' + ','.join(f'{d}' for d in column.cell_shape) + ']'
        return spec
    colspecs = ', '.join(
        [get_colspec(t.column_name, t) for t in tensors] +
        [get_colspec(c.name, c) for t in tables for c in t.columns]
    )

    taql_command = f'CREATE TABLE {table} {colspecs} LIMIT {rows} DMINFO {dminfo}'
    writer_command = ['taql', taql_command]
    print(f'\nCreating table with: {subprocess.list2cmdline(writer_command)}')
    subprocess.check_call(writer_command)
    print(f'\nTable {table} written\n')


def tensor_to_plasma(client, tensor_info):
    """Writes a single tensor into plasma"""
    object_id = plasma.ObjectID(tensor_info.object_id.encode('ascii'))
    tensor_size = pyarrow.ipc.get_tensor_size(tensor_info.tensor)
    out_buf = client.create(object_id, tensor_size)
    writer = pyarrow.FixedSizeBufferWriter(out_buf)
    pyarrow.ipc.write_tensor(tensor_info.tensor, writer)
    client.seal(object_id)
    print(f'Tensor for column {tensor_info.column_name} written to plasma, size: {tensor_size}, dtype: {tensor_info.dtype}, Object ID: {object_id}')

def tensors_to_plasma(client, args):
    """What is says on the tin"""

    if not args.tensors:
        return [], 0

    tensors = [tensor_info(t) for t in args.tensors]
    tensor_lengths = set(map(lambda t: t.tensor.shape[0], tensors))
    if len(tensor_lengths) > 1:
        parser.error("All tensors must have the same amount of data")
    tensor_length = next(iter(tensor_lengths))

    for tensor in tensors:
        tensor_to_plasma(client, tensor)
    return tensors, tensor_length

def table_to_plasma(client, table_info):
    """Writes a single table into plasma"""

    Writer = pyarrow.RecordBatchStreamWriter
    object_id = plasma.ObjectID(table_info.object_id.encode('ascii'))
    schema = table_info.table.schema

    # Get record batches, determine size sum
    max_chunksize = None # or 64 * 1024, or...
    batches = table_info.table.to_batches(max_chunksize)
    record_batch_size = sum(map(pyarrow.ipc.get_record_batch_size, batches))

    # Determine base size required for headers
    test_stream = pyarrow.BufferOutputStream()
    Writer(test_stream, schema).close()
    table_size = test_stream.tell() + record_batch_size

    # Actual table writing, record by record
    out_buf = client.create(object_id, table_size)
    buf_writer = pyarrow.FixedSizeBufferWriter(out_buf)
    with contextlib.closing(Writer(buf_writer, schema)) as writer:
        for batch in batches:
            writer.write_batch(batch)

    # Check that prediction was right - if this breaks we want to
    # learn about it early.
    if buf_writer.tell() != table_size:
        raise RuntimeError(
            f"Unexpected size of serialised table: {buf_writer.tell()} != {table_size}!"
        )
    client.seal(object_id)
    print(f'Table with columns {", ".join(c.name for c in table_info.columns)} written to plasma, size: {table_size}, batches: {len(batches)}, dtypes: {", ".join(c.dtype for c in table_info.columns)}, Object ID: {object_id}')

def tables_to_plasma(client, args):
    """What is says on the tin"""

    if not args.tables:
        return [], 0

    tables = [table_info(t) for t in args.tables]
    table_rows = set(map(lambda t: t.table.num_rows, tables))
    if len(table_rows) > 1:
        parser.error("All tables must have the same number of rows")
    table_rows = next(iter(table_rows))

    for table in tables:
        table_to_plasma(client, table)
    return tables, table_rows


def do_all(args):
    client = plasma.connect(args.plasma_socket)
    tensors, tensor_length = tensors_to_plasma(client, args)
    tables, table_rows = tables_to_plasma(client, args)
    if tensors and tables:
        if tensor_length != table_rows:
            raise ValueError(
                f'Tables and tensors have different length/rows: {tensor_length} != {table_rows}'
            )
        rows = table_rows
    elif tensors:
        rows = tensor_length
    else:
        rows = table_rows
    write_table(args.output_table, rows, tensors, tables)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--tensor', action='append', dest="tensors", help='Tensors to store in Plasma, must be of type COLNAME/type[:dim1:dim2:...]=n[,n...]', default=[])
    parser.add_argument('-T', '--table', action='append', dest="tables", help='Tables to store in Plasma, must be of type COLNAME/type[:dim1:dim2:...]=n[,n...]|COLNAME2...', default=[])
    parser.add_argument('-s', '--plasma_socket', help='The socket to use for Plasma', type=_plasma_socket, default=_plasma_socket(''))
    parser.add_argument('-S', '--dont-start-plasma', help='Use an existing plasma store', action='store_true')
    parser.add_argument('-o', '--output-table', help='Table to write', default='table.tab')
    args = parser.parse_args()

    if not args.tables and not args.tensors:
        parser.error('At least one -t/-T must be given')

    if args.dont_start_plasma:
        do_all(args)
    else:
        with Store(args.plasma_socket):
            do_all(args)
            wait_forever()

if __name__ == '__main__':
    main()