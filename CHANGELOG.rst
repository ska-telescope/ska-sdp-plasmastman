Changelog
=========

 * Migrated to use new cpp-build-base image and new $SKA_CPP_DOCKER_BUILDER_IMAGE variable.

1.3
---

 * Added support for runtime *properties*
   on the plasma storage manager.
   Two properties are supported,
   ``PLASMACONNECTRETRIES`` and ``PLASMAGETTIMEOUT``,
   making it possible to configure plasma-related aspects
   of the storage manager at runtime.
 * Added validation for user-provided Plasma Object IDs.

1.2
---

 * Added support for Arrow Table mapping.
   Individual Fields/Columns from an Arrow Table
   can be mapped to the equally named casacore Table.
   The mapping can be given
   via the new ``TABLEOBJECTIDS`` Data Manager specification property.
 * Changed ``OBJECTIDS`` Data Manager specification property name
   to ``TENSOROBJECTIDS``
   to explicitly state what type of objects do they refer to.
 * Added public C++ API documentation where missing.

1.1
---

 * Added support for generic configuration
   of the plasma storage manager
   via Data Manager Specification (casacore ``Record``) objects.
   This makes it possible to create casacore Tables
   with correctly configured plasma storage managers
   without executables built for that specific purpose.
   Most unit tests indeed
   now create Tables using ``taql``,
   which supports this generic configuration mechanism.

1.0.1
-----

 * Removed memcheck tests from GitLab CI pipeline.

1.0
---

 * First version of the plasma storage manager.
 * A single column is backed up by a single Tensor
   stored on a single Plasma store;
   multiple columns require multiple Tensors
   stored on a single Plasma store.
 * Read-only operations are supported
   for both scalar and array columns.
 * Shape and type are checked
   to ensure a Tensor can be used for a given column.
 * Zero-copy is supported for operations
   where this can be accomplished, namely:
   full-column reads (array and scalar columns),
   single, continuous row range reads (array and scalar columns), and
   single cell reads (array columns).
 * Existing programs can use this storage manager without modifications,
   as demonstrated by tests with ``taql`` and ``showtableinfo``.
 * Table creation is a manual process.
   A ``table_writer`` utility is included to help with this.
