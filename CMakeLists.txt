cmake_minimum_required(VERSION 3.5)

file(STRINGS version.txt PlasmaStMan_VERSION)
message(STATUS "Building PlasmaStMan version ${PlasmaStMan_VERSION}")

# Project configuration, specifying version, languages,
# and the C++ standard to use for the whole project
project(PlasmaStMan LANGUAGES CXX VERSION ${PlasmaStMan_VERSION})
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake/modules)
include(CTest)

# External projects
if (BUILD_TESTING)
  add_subdirectory(external/gtest-1.8.1 googletest EXCLUDE_FROM_ALL)
# installed as git submodule - if this is your first clone you need to
# git submodule init
# git submodule update
# This is a cmake module and needs no further input from you
endif()

find_package(CasaCore REQUIRED COMPONENTS tables)
add_library(casacore::tables INTERFACE IMPORTED)
target_include_directories(casacore::tables INTERFACE ${CASACORE_INCLUDE_DIR})
target_link_libraries(casacore::tables INTERFACE ${CASA_TABLES_LIBRARY} ${CASA_CASA_LIBRARY})

# They define the arrow_shared/static and plasma_shared/static targets
find_package(Arrow REQUIRED)
find_package(Plasma REQUIRED)


include(cmake/warnings.cmake)
set_project_warnings(project_warnings)

add_subdirectory(src/ska/plasma)

# Install cmake config + version + target files
include(CMakePackageConfigHelpers)
configure_package_config_file(
  cmake/PlasmaStManConfig.cmake.in
  "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PlasmaStManConfig.cmake"
  INSTALL_DESTINATION
    share/plasmastman/cmake
)
write_basic_package_version_file(
  "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PlasmaStManVersion.cmake"
  COMPATIBILITY
    AnyNewerVersion
)
install(FILES
  ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PlasmaStManConfig.cmake
  ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PlasmaStManVersion.cmake
  DESTINATION
    share/PlasmaStMan/cmake
  COMPONENT
    dev
)
install(EXPORT plasmastman-targets
  FILE
    PlasmaStManTargets.cmake
  DESTINATION
    share/PlasmaStMan/cmake
  COMPONENT
    dev
  NAMESPACE
    plasmastman::
)
