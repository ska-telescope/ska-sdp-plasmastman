Plasma Storage Manager for casacore
===================================

This project implements a casacore storage manager
that allows interfacing with existing Apache Arrow Tensor and Table objects
stored on an Apache Plasma store.

For details on installation and usage
please read the `online documentation <http://developer.skatelescope.org/projects/ska-sdp-plasmastman>`_.
